#include<thread>
#include"MessagesSender.h"
using namespace std;
int main(void)
{
	MessagesSender t;	
	t.start();// no reason for thread
	thread reader(&MessagesSender::read, &t); // send by reference
	thread sender(&MessagesSender::send, &t); // same
	system("pause");
	reader.join();// threads ends when the program get the word stop
	sender.join();// same
	return 0;
}