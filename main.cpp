#include <fstream>
#include<thread>
#include<mutex>
#include <chrono>
#include<vector>
using namespace std;
mutex m;
bool isPrime(int n)
{
	// Corner case 
	if (n <= 1)
		return false;

	// Check from 2 to n-1 
	for (int i = 2; i < n; i++)
		if (n % i == 0)
			return false;

	return true;
}
void writePrimesToFile(int begin, int end, ofstream & file)
{
	for (int i = begin; i < end; i++)
	{
		if (isPrime(i))
		{
			m.lock();
			file << i << "\n";
			m.unlock();
		}
	}
}

void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	ofstream file;
	file.open(filePath);
	vector<thread*> t;
	thread now(writePrimesToFile, 0, N, ref(file));
	now.join();
	for (int i = begin; i < end; i++)
	{
		if (i%N == 0)
		{
			t.push_back(new thread(writePrimesToFile, i, i + N, ref(file)));
		}
	}
	for (int i = 0; i < t.size(); i++)
	{
		t[i]->join();
	}
}
int main(void)
{
	callWritePrimesMultipleThreads(5, 100, "C:\\Users\\user\\Desktop\\t.txt", 10);
	return 0;
}