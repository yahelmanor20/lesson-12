#include "MessagesSender.h"


std::mutex m;
MessagesSender::MessagesSender()
{
}


MessagesSender::~MessagesSender()
{
}

void MessagesSender::start()
{
	// the start menu
	int choise = 0;
	string name;
	while (choise!=4)
	{
		cout << "1.	Signin" << endl;
		cout << "2.	Signout"<< endl;
		cout << "3.	Connected Users"<< endl;
		cout << "4.	exit"<< endl;
		cin >> choise;
		switch (choise)
		{
		case 1:
			cout << "Enter name: " << endl;
			cin >> name;
			if (users.count(name))
			{
				cout << "there is a user !" << endl;
				break;
			}
			users.insert(name);
			break;
		case 2:
			cout << "Enter name: " << endl;
			cin >> name;
			if (users.count(name) == 0)
			{
				cout << "there is a user !" << endl;
				break;
			}
			users.erase(name);
			break;
		case 3:
			for (set<string>::iterator iter = users.begin(); iter != users.end(); ++iter)
			{
				cout << (*iter) << endl;
			};
			break;
		default:
			break;
		}
	}

}

void MessagesSender::read()
{
	fstream file;
	string output;
	file.open("data.txt");
	while (true)
	{
		while (!file.eof()) { // checks the end of the file
			getline(file, output); // puts the line in string
			std::unique_lock<std::mutex>locker(m); // locks because we are using data
			data.push_back(output);
			locker.unlock();
		}
		std::ofstream ofs;
		ofs.open("data.txt", std::ofstream::out | std::ofstream::trunc); // clears the file!
		ofs.close();
		std::this_thread::sleep_for(chrono::seconds(60));
	}
	file.close();
}

void MessagesSender::send()
{
	ofstream file("output.txt");
	string tmp;
	set<string>::iterator iter;
	while (true)
	{
		for (size_t i = 0; i < this->data.size(); i++)
		{
			for (iter = users.begin(); iter != users.end(); ++iter) // print all the set of the data
			{
				tmp = data[i];
				std::unique_lock<std::mutex>locker(m);// lock the mutex
				file << (*iter) << " : " << data[i] << endl; // print to the file
				locker.unlock();
			};
		}
		std::this_thread::sleep_for(chrono::seconds(60)); // sleeps the thread
	}
	file.close();
}

