#pragma once
#include<iostream>
#include<vector>
#include<string>
#include<set>
#include<fstream>
#include <chrono>
#include <thread>
#include<mutex>
using namespace std;
class MessagesSender
{
public:
	MessagesSender();
	~MessagesSender();
	void start();
	void read();
	void send();
private:
	vector<string> data;
	set<string> users;
};

